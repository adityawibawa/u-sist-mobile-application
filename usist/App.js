import React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';


import SplashScreen from './screen/SplashScreen';
import ChatDate from './screen/ChatDate';
import HomeScreen from './screen/HomeScreen';
import Profile from './screen/Profile';
import WelcomeScreen from './screen/WelcomeScreen';
import LoginScreen from './screen/LoginScreen';
import ResetPassword from './screen/ResetPassword';
import SettingsScreen from './screen/SettingsScreen';
import ChatLibrary from './screen/ChatLibrary';
import ChatNews from './screen/ChatNews';
import ChatCareer from './screen/ChatCareer';
import ChatSupport from './screen/ChatSupport';
import ChannelList from './screen/ChannelList';
import AuthLoadingScreen from './screen/AuthLoadingScreen';

const AppNavigator = createStackNavigator({

  splash: SplashScreen,
  welcome: WelcomeScreen,


  AuthLoading: AuthLoadingScreen,

  
  login: LoginScreen,
  home: HomeScreen,
  

  settings: SettingsScreen,
  resetPass: ResetPassword,


  chatLibrary : ChatLibrary,
  chatCareer: ChatCareer,
  chatSupport: ChatSupport,
  chatDate: ChatDate,
  chatNews: ChatNews,
  channelList: ChannelList,
  
  
  profile: Profile,
  
  
  
});

export default createAppContainer(AppNavigator);