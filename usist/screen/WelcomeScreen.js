import React, {Component} from 'react';
import { StyleSheet, View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';



export default class WelcomeScreen extends Component {

    static navigationOptions = {
        headerShown: false,
    };

    onPress = () => {
        this.props.navigation.navigate('login');
    };

    
    render(){
        return (
            <SafeAreaView>
            <View style={{height: "100%", backgroundColor: '#ffffff'}}>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 30}}>
                    <Text style={{fontWeight: "bold", textAlign: "center", fontSize: 50, marginBottom: 20, letterSpacing: 10}}>U - SIST</Text>
                    <Text style={{fontWeight: "900", textAlign: "center", fontSize: 50, letterSpacing: 10}}>Welcome</Text>
                </View>
                <View style={{flex: 3, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{ textAlign: "center", fontSize: 12, letterSpacing: 2}}>Having a personalised assistance for</Text>
                    <Text style={{ textAlign: "center", fontSize: 12, letterSpacing: 2}}>better uni-life.</Text>

                    <Text style={{ textAlign: "center", fontSize: 12, marginTop: 5, letterSpacing: 2}}>To do this, we need you to Log in your </Text>
                    <Text style={{ textAlign: "center", fontSize: 12, letterSpacing: 2}}>username and password.</Text>

                    <Image
                    style={{height: 200, width: 200, marginTop: 10}}
                    source={require('../img/logo.jpg')}
                    />
                </View>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <TouchableOpacity onPress={this.onPress} style={{alignItems: "center", backgroundColor: "#DDDDDD", padding: 10}}>
                    <Text style={{marginTop: 5, marginBottom: 5, marginLeft: 70, marginRight: 70}}>Log In</Text>
                </TouchableOpacity>
                <Text style={{textAlign: "center", color: "red", marginTop: 10}}>Already have an account?</Text>
                </View>
            </View>
            </SafeAreaView>
        );
    }
}