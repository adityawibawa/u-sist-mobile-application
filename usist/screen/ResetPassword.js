import React, {Component} from 'react';
import { StyleSheet, TextInput, View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';



export default class ResetPassword extends Component {

    static navigationOptions = {
        headerShown: false,
    };

    onPress = () => {
        this.props.navigation.navigate('chatDate');
    };

    handleChange = key => val =>{
        this.setState({[key]:val})
    }
    
    render(){
        return (
            <SafeAreaView>
            <View style={{height: "100%", backgroundColor: '#ffffff'}}>
                <View style={{flex: 1, alignItems: 'center', justifyContent: "flex-start", marginTop: 50, marginHorizontal: 20}}>
                    <Text style={{fontWeight: "900", textAlign: "center", fontSize: 25, letterSpacing: 5}}>Password</Text>
                    <TextInput 
                        placeholder="New Password"
                        style= {{
                          
                          marginTop: 20,
                          borderWidth: 1,
                          borderColor: '#fff',
                          borderBottomColor: "black",
                          width: '90%',
                          marginBottom: 10,
                        }}
                        onChangeText = {this.handleChange('phone')}
                    />
                    <TextInput 
                        placeholder="Confirm New Password"
                        style= {{
                          
        
                          borderWidth: 1,
                          borderColor: '#fff',
                          borderBottomColor: "black",
                          width: '90%',
                          marginBottom: 20,
                        }}
                        onChangeText = {this.handleChange('phone')}
                    />
                </View>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-end', alignItems: "center", marginBottom: 30}}>
                    <TouchableOpacity onPress={this.onPress} style={{alignItems: "center", backgroundColor: "#DDDDDD", padding: 10}}>
                        <Text style={{marginTop: 5, marginBottom: 5, marginLeft: 70, marginRight: 70}}>Verify Password</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </SafeAreaView>
        );
    }
}