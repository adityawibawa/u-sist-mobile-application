import React, {Component} from 'react';
import { StyleSheet, View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const steps = [
    {
      id: '0',
      message: 'Welcome to react chatbot!',
      trigger: '1',
    },
    {
      id: '1',
      message: 'Bye!',
      end: true,
    },
  ];

  
const cogIcon = <Icon name="cog" size={30} color="#fff" />;
const profileIcon = <Icon name="user-circle" size={30} color="#fff" />;
  

const styles = StyleSheet.create({
container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 70,
    backgroundColor: '#ffffff',
},
item: {
    width: '50%',
    padding: 10,
}
})

export default class HomeScreen extends Component {


    onPressDates = () => {
        this.props.navigation.navigate('chatDate');
    };

    onPressLibrary = () => {
        this.props.navigation.navigate('chatLibrary');
    };

    onPressCareer = () => {
        this.props.navigation.navigate('chatCareer');
    };

    onPressNews = () => {
        this.props.navigation.navigate('chatNews');
    };

    onPressSupport = () => {
        this.props.navigation.navigate('chatSupport');
    };

    onPressChannel = () => {
        this.props.navigation.navigate('channelList');
    };

    static navigationOptions = ({navigation}) => {
        return {
            title: 'U-SIST',
            headerStyle: {
                backgroundColor: '#000000',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
                alignSelf: 'center',
            },
            headerRight: (
                <TouchableOpacity onPress={() => navigation.navigate('profile')}>
                    {profileIcon}
                </TouchableOpacity>
            ),
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.navigate('settings')} style={{marginLeft: 10}}>
                    {cogIcon}
                </TouchableOpacity>
            )
        }
    }
    

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.item}>
                    <TouchableOpacity onPress={this.onPressDates}>
                        <Image
                            style={{height: 120, width: 120}}
                            source={require('../img/menu-dates.png')}
                        />
                    </TouchableOpacity>
                    
                </View>
                <View style={styles.item}>
                    <TouchableOpacity onPress={this.onPressLibrary}>
                        <Image
                            style={{height: 120, width: 120}}
                            source={require('../img/menu-library.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.item}>
                    <TouchableOpacity onPress={this.onPressCareer}>
                        <Image
                            style={{height: 120, width: 120}}
                            source={require('../img/menu-career.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.item}>
                    <TouchableOpacity onPress={this.onPressNews}>
                        <Image
                            style={{height: 120, width: 120}}
                            source={require('../img/menu-news.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.item}>
                    <TouchableOpacity onPress={this.onPressSupport}>
                        <Image
                            style={{height: 120, width: 120}}
                            source={require('../img/menu-support.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.item}>
                    <TouchableOpacity onPress={this.onPressChannel}>
                        <Image
                            style={{height: 120, width: 120}}
                            source={require('../img/menu-channel.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>

        );
    }
}