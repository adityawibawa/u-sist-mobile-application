import React, {Component} from 'react';
import { StyleSheet, TextInput, View, Text, Image, Dimensions, TouchableOpacity, AsyncStorage, } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import User from '../User'
import firebase from 'firebase'


export default class LoginScreen extends Component {

    static navigationOptions = {
        headerShown: false,
    };

    state = {
        username: '',
        password: ''
    }

    componentWillMount() {
        AsyncStorage.getItem('userPassword').then(val => {
            if(val){
                this.setState({password:val})
            }
        })
    }

    onPress = () => {
        this.props.navigation.navigate('home');
    };

    handleChange = key => val =>{
        this.setState({[key]:val})
    }

    submitForm = async () => {
        if (this.state.username == '' | this.state.password == ''){
            alert(this.state.password + '\n'+this.state.username)
        }else{
            await AsyncStorage.setItem('userPassword', this.state.password)
            User.password = this.state.password
            User.username = this.state.username
            firebase.database().ref('users/'+User.password).set({username:this.state.username})

            this.props.navigation.navigate('home')
            alert('user successfully saved')
        }
    }
    
    render(){
        return (
            <SafeAreaView>
            <View style={{height: "100%", backgroundColor: '#ffffff'}}>
                <View style={{flex: 1, alignItems: 'center', justifyContent: "flex-start", marginTop: 50, marginHorizontal: 20}}>
                    <Text style={{fontWeight: "900", textAlign: "center", fontSize: 25, letterSpacing: 5}}>Log in</Text>
                    <TextInput 
                        placeholder="Username"
                        style= {{
                          
                          marginTop: 20,
                          borderWidth: 1,
                          borderColor: '#fff',
                          borderBottomColor: "black",
                          width: '90%',
                          marginBottom: 10,
                        }}
                        onChangeText = {this.handleChange('username')}
                    />
                    <TextInput 
                        placeholder="Password"
                        style= {{
                          
        
                          borderWidth: 1,
                          borderColor: '#fff',
                          borderBottomColor: "black",
                          width: '90%',
                          marginBottom: 20,
                        }}
                        onChangeText = {this.handleChange('password')}
                    />
                    <Text style={{textAlign: "center", color: "red", marginTop: 10}}>Forgot Password</Text>
                </View>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-end', alignItems: "center", marginBottom: 30}}>
                    <TouchableOpacity onPress={this.submitForm} style={{alignItems: "center", backgroundColor: "#DDDDDD", padding: 10}}>
                        <Text style={{marginTop: 5, marginBottom: 5, marginLeft: 70, marginRight: 70}}>Log In</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </SafeAreaView>
        );
    }
}