import React, {Component} from 'react';
import {View, StyleSheet, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import ChatBot from 'react-native-chatbot';
import Icon from 'react-native-vector-icons/FontAwesome';
import { SafeAreaView } from 'react-native-safe-area-context';


const steps = [
    {
      id: '0',
      message: 'Welcome to react chatbot!',
      trigger: '1',
    },
    {
      id: '1',
      user: true,
      trigger: '2',
    },
    {
      id: '2',
      user: true,
      end: true,
    },
  ];

  
  const styles = StyleSheet.create({
    container: {
        flex: 9,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 70,
        backgroundColor: '#ffffff',
    },
    item: {
        width: '50%',
        padding: 10,
    }
    })
  

  const leftIcon = <Icon name="chevron-left" size={30} color="#fff" />;
  const historyIcon = <Icon name="history" size={30} color="#fff" />;
  const hideProfile = true;
  

  export default class ChannelList extends Component {

    static navigationOptions = ({navigation}) => {
      return {
        title: 'U-SIST',
        headerStyle: {
            backgroundColor: '#5A189A',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
            alignSelf: 'center',
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.navigate('home')} style={{marginLeft: 10}}>
                {leftIcon}
            </TouchableOpacity>
        ),
      }
  }

    render(){
        return (
            <SafeAreaView>
              <View style={{height: "100%"}}>
                <View style={styles.container}>
                  <View style={styles.item}>
                      <TouchableOpacity onPress={this.onPressDates}>
                          <Image
                              style={{height: 120, width: 120}}
                              source={require('../img/addnew.png')}
                          />
                      </TouchableOpacity>
                      
                  </View>
                  <View style={styles.item}>
                      <TouchableOpacity onPress={this.onPressLibrary}>
                          <Image
                              style={{height: 120, width: 120}}
                              source={require('../img/moodle.png')}
                          />
                      </TouchableOpacity>
                  </View>
                  <View style={styles.item}>
                      <TouchableOpacity onPress={this.onPressCareer}>
                          <Image
                              style={{height: 120, width: 120}}
                              source={require('../img/fiori.png')}
                          />
                      </TouchableOpacity>
                  </View>
                  <View style={styles.item}>
                      <TouchableOpacity onPress={this.onPressNews}>
                          <Image
                              style={{height: 120, width: 120}}
                              source={require('../img/gmeet.png')}
                          />
                      </TouchableOpacity>
                  </View>
                  <View style={styles.item}>
                      <TouchableOpacity onPress={this.onPressSupport}>
                          <Image
                              style={{height: 120, width: 120}}
                              source={require('../img/zoom.png')}
                          />
                      </TouchableOpacity>
                  </View>
                  <View style={styles.item}>
                      <TouchableOpacity onPress={this.onPressChannel}>
                          <Image
                              style={{height: 120, width: 120}}
                              source={require('../img/gclass.png')}
                          />
                      </TouchableOpacity>
                  </View>
              </View>
                <View style={{flex: 1, flexDirection: "row", backgroundColor: "#5A189A", justifyContent: "center", alignItems: "center"}}>
                  <View style={{marginLeft: -40, marginRight: 40, alignItems: "center"}}>
                  <Image
                    style={{height: 30, width: 30}}
                    source={require('../img/icon-channel.png')}
                />
                    <Text style={{color: "white"}}>Channel</Text>
                  </View>
                  <View style={{marginLeft: 40, marginRight: -40, alignItems: "center"}}>
                    {historyIcon}
                    <Text style={{color: "white"}}>Recent</Text>
                  </View>
                </View>
              </View>
            </SafeAreaView>
        );
    }
}