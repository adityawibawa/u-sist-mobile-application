import React, {Component} from 'react';
import {View, StyleSheet, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import ChatBot from 'react-native-chatbot';
import Icon from 'react-native-vector-icons/FontAwesome';
import {notifikasi} from '../src/Notifikasi'




  const styles = StyleSheet.create({
    footer:{
        backgroundColor: '#2FBF71',
        
    },
    inputStyles:{
        backgroundColor: '#fff',
        borderRadius: 500,
        height: 40,
        width: 320
    },
    buttonStyle:{
      backgroundColor: '#2FBF71',
    },
    })
  
  

  const leftIcon = <Icon name="chevron-left" size={30} color="#fff" />;
  const sendButton = <Icon name="paper-plane" size={30} color="#fff" />;
  const hideProfile = true;
  

  export default class ChatCareer extends Component {

    static navigationOptions = ({navigation}) => {
      return {
        title: 'U-SIST',
        headerStyle: {
            backgroundColor: '#2FBF71',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
            alignSelf: 'center',
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.navigate('home')} style={{marginLeft: 10}}>
                {leftIcon}
            </TouchableOpacity>
        ),
      }
  }
  constructor(props) {
    super(props);

    this.state = {
        data: [],
        steps: [],
       
    };
  }

  componentWillMount(){

    
     
    fetch('http://18.118.154.146:8000/washer/')

    .then((response) => response.json())
    .then((json) => {
        this.setState({ data: json });

        
    })
    .catch((error) => console.error(error))
    .finally(() => {
        this.setState({ isLoading: false });
    });

    const userMessage = [];
            
      this.setState({
        steps: [
          {
            id: '0',
            message: 'Welcome to Career Chatbot!',
            trigger: '1',
          },
          {
            id: '1',
            message: "Type keyword to find (lowercase)",
            trigger: '2',
          },
          {
            id: '2',
            user: true,
            trigger: '3',
          },
          {
            id: '3',
            message: ({ previousValue, steps }) => {
              notifikasi.configure();
              notifikasi.buatChannel("1");
              notifikasi.kirimNotifikasi("1", "New Notification From Career Chatbot", "1 New Message");

              userMessage.push(previousValue)
              var result = ""
      
                  if (previousValue.includes("gpa")) {
                      
                        result += "Your GPA is" + '\n' + "3.88" + '\n' + "Earned Credit: 84" + '\n' + "Total Semester : 4" 
                        return result

                  } else if (previousValue.includes("intermediate")) {
                        result += "Intermediate Programming" + '\n' + "A"
                        return result

                  } else {
                        return "Not Found Based on Your Keyword"
                  }
                
            },
            trigger: '4'
          },
          {
            id: '4',
            message: "what other things could I help you?",
            trigger: '5'
          },
          {
            id: '5',
            options: [
              { value: 'theend', label: 'Enough', trigger: '6' },
              { value: 'continue', label: 'Find Another', trigger: '1' },
            ],
          },
          {
            id: '6',
            message: "Thank You!",
            end: true
          },
        ]
      })
      
      
      
      
  }  

    render(){
        return (
            <ChatBot steps={this.state.steps} 
                botBubbleColor= "#ffffff"
                botFontColor="#000"
                botAvatar= 'https://eucaria.id/wp-content/uploads/2021/07/career.jpg'
                hideUserAvatar = {hideProfile}
                placeholder = "Type a question"
                footerStyle={styles.footer}
                inputStyle={styles.inputStyles}
                submitButtonContent = {sendButton}
                submitButtonStyle = {styles.buttonStyle}
            />
        );
    }
}