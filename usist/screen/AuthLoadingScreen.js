import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import firebase from 'firebase';
import User from '../User'

export default class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this._bootsrapAsync();
    }

    componentWillMount(){
        const firebaseConfig = {
            apiKey: "AIzaSyA-haV71CV9f2dG6jLxyfDkyHm11SmWqBg",
            authDomain: "usist-4e87d.firebaseapp.com",
            projectId: "usist-4e87d",
            storageBucket: "usist-4e87d.appspot.com",
            messagingSenderId: "281272676099",
            appId: "1:281272676099:web:550d6ec0be69ee82660336",
            measurementId: "G-X0J1DKZE7K"
          };
          // Initialize Firebase
          if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        }

    }

    _bootsrapAsync = async () => {
        User.password = await AsyncStorage.getItem('userPassword');

        this.props.navigation.navigate(User.password ? 'home' : 'welcome');
    };

    render() {
        return (
            <View>
                <ActivityIndicator />
                <StatusBar barStyle="default" />
            </View>
        );
    }
}