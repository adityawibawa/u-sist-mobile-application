import React, {Component} from 'react';
import {View, StyleSheet, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import ChatBot from 'react-native-chatbot';
import Icon from 'react-native-vector-icons/FontAwesome';
import {notifikasi} from '../src/Notifikasi'




  const styles = StyleSheet.create({
    footer:{
        backgroundColor: '#FFD503',
        
    },
    inputStyles:{
        backgroundColor: '#fff',
        borderRadius: 500,
        height: 40,
        width: 320
    },
    buttonStyle:{
      backgroundColor: '#FFD503',
    },
    })
  
  
  
  

  const leftIcon = <Icon name="chevron-left" size={30} color="#fff" />;
  const sendButton = <Icon name="paper-plane" size={30} color="#fff" />;
  const hideProfile = true;
  

  export default class ChatNews extends Component {

    static navigationOptions = ({navigation}) => {
      return {
        title: 'U-SIST',
        headerStyle: {
            backgroundColor: '#FFD503',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
            alignSelf: 'center',
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.navigate('home')} style={{marginLeft: 10}}>
                {leftIcon}
            </TouchableOpacity>
        ),
      }
  }

  constructor(props) {
    super(props);

    this.state = {
        data: [],
        steps: [],
       
    };
  }

  componentWillMount(){

    
     
    fetch('http://18.118.154.146:8000/berita/')

    .then((response) => response.json())
    .then((json) => {
        this.setState({ data: json });

        
    })
    .catch((error) => console.error(error))
    .finally(() => {
        this.setState({ isLoading: false });
    });

    const userMessage = [];
            
      this.setState({
        steps: [
          {
            id: '0',
            message: 'Welcome to News Chatbot!',
            trigger: '1',
          },
          {
            id: '1',
            message: "Which keyword do you want to use to find news?",
            trigger: '2',
          },
          {
            id: '2',
            options: [
              { value: 'title', label: 'Title', trigger: '3' },
              { value: 'category', label: 'Category', trigger: '4' },
            ],
          },
          {
            id: '3',
            user: true,
            trigger: '5',
          },
          {
            id: '4',
            user: true,
            trigger: '6',
          },
          {
            id: '5',
            message: ({ previousValue, steps }) => {
              notifikasi.configure();
              notifikasi.buatChannel("1");
              notifikasi.kirimNotifikasi("1", "New Notification From News Chatbor", "1 New Message");

              userMessage.push(previousValue)
              var result = ""
              if (this.state.data.length > 0) {
                for (let index = 0; index < this.state.data.length; index++) {
                  if (this.state.data[index].judul.includes(previousValue)) {
        
                        result += this.state.data[index].judul + '\n' + this.state.data[index].deskripsi + '\n' + this.state.data[index].kategori + '\n' + '\n'
                      
                  }
                }
                if(result==""){
                  return "News Not Found"
                } else{
                  return result
                }
              }
            },
            trigger: '7'
          },
          {
            id: '6',
            message: ({ previousValue, steps }) => {
              userMessage.push(previousValue)
              var result = ""
              if (this.state.data.length > 0) {
                for (let index = 0; index < this.state.data.length; index++) {
                  if (this.state.data[index].kategori.includes(previousValue)) {
        
                        result += this.state.data[index].judul + '\n' + this.state.data[index].deskripsi + '\n' + this.state.data[index].kategori + '\n' + '\n'
                      
                  }
                }
                if(result==""){
                  return "News Not Found"
                } else{
                  return result
                }
              }
            },
            trigger: '7'
          },
          {
            id: '7',
            message: "what other things could I help you?",
            trigger: '8'
          },
          {
            id: '8',
            options: [
              { value: 'theend', label: 'Enough', trigger: '9' },
              { value: 'continue', label: 'Help Another', trigger: '1' },
            ],
          },
          {
            id: '9',
            message: "Thank You!",
            end: true
          },
        ]
      })
      
      
  }  

    render(){
        return (
            <ChatBot steps={this.state.steps} 
                botBubbleColor= "#ffffff"
                botFontColor="#000"
                botAvatar= 'https://eucaria.id/wp-content/uploads/2021/07/news.jpg'
                hideUserAvatar = {hideProfile}
                placeholder = "Type a question"
                footerStyle={styles.footer}
                inputStyle={styles.inputStyles}
                submitButtonContent = {sendButton}
                submitButtonStyle = {styles.buttonStyle}
            />
        );
    }
}