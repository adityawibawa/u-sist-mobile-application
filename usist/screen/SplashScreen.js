import React, {Component} from 'react';
import {View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';

const THREE_SECONDS = 3000;

export default class SplashScreen extends Component {
    static navigationOptions = {
        headerShown: false,
    };


    async componentDidMount(){
        setTimeout(() => {
            this.props.navigation.navigate('AuthLoading');
        }, THREE_SECONDS);
        
    }


    render(){
        return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#ffffff'}}>
            <View style={{flex: 10, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontWeight: "bold", textAlign: "center", fontSize: 50, marginBottom: 20}}>U - SIST</Text>
                <Image
                    style={{height: 200, width: 200}}
                    source={require('../img/logo.jpg')}
                />
            </View>
        </View>
        );
    }
}