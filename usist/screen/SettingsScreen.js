import React, {Component} from 'react';
import { StyleSheet, View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },
})

const leftIcon = <Icon name="chevron-left" size={30} color="#000" />;
const saveIcon = <Icon name="check" size={30} color="#000" />;

export default class SettingsScreen extends Component {


    onPress = () => {
        this.props.navigation.navigate('home');
    };

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Setting',
            headerStyle: {
                backgroundColor: '#fff',
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            headerRight: (
                <TouchableOpacity onPress={this.onPress} style={{marginRight: 10}}>
                    {saveIcon}
                </TouchableOpacity>
            ),
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.navigate('home')} style={{marginLeft: 10}}>
                    {leftIcon}
                </TouchableOpacity>
            )
        }
    }
    

    render(){
        return (
            <View style={{height:"100%"}}>
                <View style={{flex: 3, backgroundColor: "white", padding: 50}}>
                        <Text style={{textAlign: "left"}}>Languages</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>English</Text>

                        <Text style={{textAlign: "left"}}>Notifications</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>Turn On</Text>

                        <Text style={{textAlign: "left"}}>Audio voiceover</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>Turn Off</Text>

                        <Text style={{textAlign: "left"}}>Display</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>Bright</Text>

                        <Text style={{textAlign: "left"}}>Font</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>Roboto</Text>

                        <Text style={{textAlign: "left"}}>Text Size</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>12</Text>
                </View>
            </View>

        );
    }
}
