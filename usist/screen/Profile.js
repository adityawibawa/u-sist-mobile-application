import React, {Component} from 'react';
import { StyleSheet, View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import User from'../User'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },
})

const leftIcon = <Icon name="chevron-left" size={30} color="#000" />;
const saveIcon = <Icon name="check" size={30} color="#000" />;

export default class Profile extends Component {


    onPress = () => {
        this.props.navigation.navigate('chatDate');
    };

    state = {
        username: User.username,
        password: User.password

    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Profile',
            headerStyle: {
                backgroundColor: '#fff',
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            headerRight: (
                <TouchableOpacity onPress={this.onPress} style={{marginRight: 10}}>
                    {saveIcon}
                </TouchableOpacity>
            ),
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.navigate('home')} style={{marginLeft: 10}}>
                    {leftIcon}
                </TouchableOpacity>
            )
        }
    }
    

    render(){
        console.log(User.password)
        console.log(User.username)
        return (
            <View style={{height:"100%"}}>
                <View style={styles.container}>
                        <Image
                            style={{height: 150, width: 150, marginTop: 30}}
                            source={require('../img/profile-image.png')}
                        />
                </View>
                <View style={{flex: 3, backgroundColor: "white", padding: 50}}>
                        <Text style={{textAlign: "left"}}>Name</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>{User.username}</Text>

                        <Text style={{textAlign: "left"}}>Date Of Birth</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>18 January 2003</Text>

                        <Text style={{textAlign: "left"}}>Phone Number</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>081235634278</Text>

                        <Text style={{textAlign: "left"}}>Email Address</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>{User.username}@mail.com</Text>

                        <Text style={{textAlign: "left"}}>Password</Text>
                        <Text style={{textAlign: "left", borderBottomColor: "black", borderBottomWidth: 1, marginTop: 15, marginBottom: 20}}>{User.password}</Text>
                </View>
            </View>

        );
    }
}
